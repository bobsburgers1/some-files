/* see LICENSE for copyright and license */

#ifndef CONFIG_H
#define CONFIG_H

/** modifiers **/
#define MOD1            Mod1Mask    /* ALT key */
#define MOD4            Mod4Mask    /* Super/Windows key         During compile, the Apple cmd key wasn't recognized*/
#define CONTROL         ControlMask /* Control key */
#define SHIFT           ShiftMask   /* Shift key */

/** generic settings **/
#define MASTER_SIZE     0.52
#define SHOW_PANEL      True      /* show panel by default on exec */
#define TOP_PANEL       True      /* False means panel is on bottom */
#define PANEL_HEIGHT    18        /* 0 for no space for panel, thus no panel */
#define DEFAULT_MODE    TILE      /* initial layout/mode: TILE MONOCLE BSTACK GRID FLOAT */
#define ATTACH_ASIDE    False      /* False means new window is master */
#define FOLLOW_WINDOW   False     /* follow the window when moved to a different desktop */
#define FOLLOW_MONITOR  False     /* follow the window when moved to a different monitor */
#define FOLLOW_MOUSE    True     /* focus the window the mouse just entered                        CHANGED*/
#define CLICK_TO_FOCUS  False     /* focus an unfocused window when clicked           CHANGED FOR APPLE TOUCHPAD*/
#define FOCUS_BUTTON    Button1   /* mouse button to be used along with CLICK_TO_FOCUS */
#define BORDER_WIDTH    2         /* window border width                                            CHANGED*/
#define FOCUS           "#D80606" /* focused window border color                                    CHANGED*/
#define UNFOCUS         "#000000" /* unfocused window border color                                  CHANGED*/
#define INFOCUS         "#bbc2cf" /* focused window border color on unfocused monitor */
#define MINWSZ          50        /* minimum window size in pixels */
#define DEFAULT_MONITOR 0         /* the monitor to focus initially */
#define DEFAULT_DESKTOP 1         /* the desktop to focus initially                                 CHANGED*/
#define DESKTOPS        4         /* number of desktops - edit DESKTOPCHANGE keys to suit */
#define USELESSGAP      0        /* size of ridiculously useless but pretty gaps between windows    CHANGED*/

/**
 * open applications to specified monitor and desktop
 * with the specified properties.
 * if monitor is negative, then current is assumed
 * if desktop is negative, then current is assumed
 */
static const AppRule rules[] = { \
    /*  class     monitor  desktop  follow  float */
    { "MPlayer",     0,       3,    True,   False },
    { "Gimp",        1,       0,    False,  True  },
};

/* helper for spawning shell commands */
#define SHCMD(cmd) {.com = (const char*[]){"/bin/sh", "-c", cmd, NULL}}

/**
 * custom commands
 * must always end with ', NULL };'
 */
static const char *termcmd[] = { "xfce4-terminal", NULL };
static const char *launchcmd[] = { "dmenu_run", NULL };
static const char *editor[] = { "geany", NULL };
static const char *browser1[] = { "firefox", NULL };
static const char *browser2[] = { "firefox-esr", NULL };
static const char *discord[] = { "xfce4-power-manager", NULL };
/*
#define MONITORCHANGE(K,N)                                          \
    {  MOD4,             K,              change_monitor, {.i = N}}, \
    {  MOD4|ShiftMask,   K,              client_to_monitor, {.i = N}},

#define DESKTOPCHANGE(K,N) \
    {  MOD1,             K,              change_desktop, {.i = N}}, \
    {  MOD1|ShiftMask,   K,              client_to_desktop, {.i = N}},
*/
/**
 * keyboard shortcuts
 */
static Key keys[] = {
    /* modifier          key            function           argument */
    {  MOD1,             XK_p,          spawn,             {.com = launchcmd}},
    {  MOD1|SHIFT,       XK_Return,     spawn,             {.com = termcmd}},
    {  MOD1,             XK_9,          togglepanel,       {NULL}},
    {  MOD1,             XK_Return,     rotate,            {.i = -1}},
    {  MOD1,             XK_a,          resize_stack,      {.i = -10}}, /* shrink   size in px */
    {  MOD1,             XK_s,          resize_stack,      {.i = +10}}, /* grow     size in px */
    {  MOD1|SHIFT,       XK_c,          killclient,        {NULL}},
    {  MOD1|SHIFT,       XK_r,          quit,              {.i = 0}}, /* quit with exit value 0 */
    {  MOD1|SHIFT,       XK_q,          quit,              {.i = 1}}, /* quit with exit value 1 */
/* Above ^^^ was changed/or created by me*/
    {  MOD1,             XK_BackSpace,  focusurgent,       {NULL}},
    {  MOD1,             XK_j,          next_win,          {NULL}},
    {  MOD1,             XK_k,          prev_win,          {NULL}},
    {  MOD1,             XK_h,          resize_master,     {.i = -10}}, /* decrease size in px */
    {  MOD1,             XK_l,          resize_master,     {.i = +10}}, /* increase size in px */
    {  MOD1|CONTROL,     XK_h,          rotate,            {.i = -1}},
    {  MOD1|CONTROL,     XK_l,          rotate,            {.i = +1}},
    {  MOD1|SHIFT,       XK_h,          rotate_filled,     {.i = -1}},
    {  MOD1|SHIFT,       XK_l,          rotate_filled,     {.i = +1}},
    {  MOD1,             XK_Tab,        last_desktop,      {NULL}},
    {  MOD1,             XK_space,      swap_master,       {NULL}},
    {  MOD1|SHIFT,       XK_j,          move_down,         {NULL}},
    {  MOD1|SHIFT,       XK_k,          move_up,           {NULL}},
    {  MOD1,             XK_t,          switch_mode,       {.i = TILE}},
    {  MOD1,             XK_m,          switch_mode,       {.i = MONOCLE}},
    {  MOD1,             XK_b,          switch_mode,       {.i = BSTACK}},
    {  MOD1,             XK_g,          switch_mode,       {.i = GRID}},
    {  MOD1,             XK_f,          switch_mode,       {.i = FLOAT}},
    {  MOD1,             XK_e,          spawn,             {.com = editor}},
    {  MOD1,             XK_w,          spawn,             {.com = browser1}},
    {  MOD1|SHIFT,       XK_w,          spawn,             {.com = browser2}},
    {  MOD1,             XK_d,          spawn,             {.com = discord}},
    //{  MOD4,             XK_j,          moveresize,        {.v = (int []){   0,  25,   0,   0 }}}, /* move down  */
    //{  MOD4,             XK_k,          moveresize,        {.v = (int []){   0, -25,   0,   0 }}}, /* move up    */
    //{  MOD4,             XK_l,          moveresize,        {.v = (int []){  25,   0,   0,   0 }}}, /* move right */
    //{  MOD4,             XK_h,          moveresize,        {.v = (int []){ -25,   0,   0,   0 }}}, /* move left  */
    //{  MOD4|SHIFT,       XK_j,          moveresize,        {.v = (int []){   0,   0,   0,  25 }}}, /* height grow   */
    //{  MOD4|SHIFT,       XK_k,          moveresize,        {.v = (int []){   0,   0,   0, -25 }}}, /* height shrink */
    //{  MOD4|SHIFT,       XK_l,          moveresize,        {.v = (int []){   0,   0,  25,   0 }}}, /* width grow    */
    //{  MOD4|SHIFT,       XK_h,          moveresize,        {.v = (int []){   0,   0, -25,   0 }}}, /* width shrink  */
    {  MOD1,             XK_1,          change_desktop,    {.i = 1}},
    {  MOD1|ShiftMask,   XK_1,          client_to_desktop, {.i = 1}},
    {  MOD1,             XK_2,          change_desktop,    {.i = 2}},
    {  MOD1|ShiftMask,   XK_2,          client_to_desktop, {.i = 2}},
    {  MOD1,             XK_3,          change_desktop,    {.i = 3}},
    {  MOD1|ShiftMask,   XK_3,          client_to_desktop, {.i = 3}},
    {  MOD1,             XK_4,          change_desktop,    {.i = 4}},
    {  MOD1|ShiftMask,   XK_4,          client_to_desktop, {.i = 4}},
};

/**
 * mouse shortcuts
 */
static Button buttons[] = {
    {  MOD1,    Button1,     mousemotion,   {.i = MOVE}},
    {  MOD1,    Button3,     mousemotion,   {.i = RESIZE}},
};
#endif

/* vim: set expandtab ts=4 sts=4 sw=4 : */
