#!/usr/bin/bash
# Simple script to keep my thunar and thunar-volman settings consitent.
# 
# Version  : Thunar 1.8.14
# Version  : Thunar-volman 0.9.5
# Date    : 30-APR-2021
# 
# Thunar Settings
xfconf-query -c thunar -p /default-view --create -t string -s ThunarDetailsView
xfconf-query -c thunar -p /last-details-view-column-order --create -t string -s THUNAR_COLUMN_NAME,THUNAR_COLUMN_SIZE,THUNAR_COLUMN_SIZE_IN_BYTES,THUNAR_COLUMN_DATE_MODIFIED,THUNAR_COLUMN_TYPE,THUNAR_COLUMN_OWNER,THUNAR_COLUMN_PERMISSIONS,THUNAR_COLUMN_DATE_ACCESSED,THUNAR_COLUMN_GROUP,THUNAR_COLUMN_MIME_TYPE
xfconf-query -c thunar -p /last-details-view-column-widths --create -t string -s 142,142,90,168,216,209,123,72,129,139
xfconf-query -c thunar -p /last-details-view-fixed-columns --create -t bool -s false
xfconf-query -c thunar -p /last-details-view-visible-columns --create -t string -s THUNAR_COLUMN_DATE_ACCESSED,THUNAR_COLUMN_DATE_MODIFIED,THUNAR_COLUMN_GROUP,THUNAR_COLUMN_MIME_TYPE,THUNAR_COLUMN_NAME,THUNAR_COLUMN_OWNER,THUNAR_COLUMN_PERMISSIONS,THUNAR_COLUMN_SIZE,THUNAR_COLUMN_TYPE
xfconf-query -c thunar -p /last-details-view-zoom-level --create -t string -s THUNAR_ZOOM_LEVEL_38_PERCENT
xfconf-query -c thunar -p /last-icon-view-zoom-level --create -t string -s THUNAR_ZOOM_LEVEL_100_PERCENT
xfconf-query -c thunar -p /last-location-bar --create -t string -s ThunarLocationEntry
xfconf-query -c thunar -p /last-separator-position --create -t string -s 170
xfconf-query -c thunar -p /last-show-hidden --create -t bool -s true
xfconf-query -c thunar -p /last-side-pane --create -t string -s ThunarShortcutsPane
xfconf-query -c thunar -p /last-statusbar-visible --create -t bool -s true
xfconf-query -c thunar -p /last-view --create -t string -s ThunarDetailsView
xfconf-query -c thunar -p /last-window-height --create -t string -s 777
xfconf-query -c thunar -p /last-window-maximized --create -t bool -s false
xfconf-query -c thunar -p /last-window-width --create -t string -s 702
xfconf-query -c thunar -p /misc-date-style --create -t string -s THUNAR_DATE_STYLE_MMDDYYYY
xfconf-query -c thunar -p /misc-middle-click-in-tab --create -t bool -s true
xfconf-query -c thunar -p /misc-show-delete-action --create -t bool -s true
xfconf-query -c thunar -p /misc-single-click --create -t bool -s false
xfconf-query -c thunar -p /misc-text-beside-icons --create -t bool -s true
xfconf-query -c thunar -p /misc-thumbnail-mode --create -t string -s THUNAR_THUMBNAIL_MODE_ALWAYS
xfconf-query -c thunar -p /shortcuts-icon-emblems --create -t bool -s true
xfconf-query -c thunar -p /shortcuts-icon-size --create -t string -s THUNAR_ICON_SIZE_32
xfconf-query -c thunar -p /tree-icon-size --create -t string -s THUNAR_ICON_SIZE_16
#
# Thunar Hidden Settings I Use
xfconf-query -c thunar -p /misc-full-path-in-title --create -t bool -s true
xfconf-query -c thunar -p /misc-small-toolbar-icons --create -t bool -s true
xfconf-query -c thunar -p /misc-switch-to-new-tab --create -t bool -s false
#
# Thunar-Volman Settings
xfconf-query -c thunar -p /autobrowse/enabled --create -t bool -s false
xfconf-query -c thunar -p /automount-drives/enabled --create -t bool -s true
xfconf-query -c thunar -p /automount-media/enabled --create -t bool -s true
xfconf-query -c thunar -p /autoopen/enabled --create -t bool -s false
