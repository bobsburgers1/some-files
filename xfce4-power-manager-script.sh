#!/bin/sh
# Wrote this script when looking up how to turn the brighness keys on my 2008
# MacBook.
#
# When starting the xfce4-power-manager I was having a problem where the manager would 
# start but the brighntess keys wouldn't work. By using the -c flag the keys would 
# initialize 50% percent of the time. Restarting the program wouldn't work either. 
# I would have to quit my window manager, and then start the program again. Plus, the 
# -c flag opens the manager evertime and that was annoying.
# 
# I found that running the following command would turn the keys on after initialized.
# EDIT : No .....nevermind it still doesn't initialize the keys .... But I ended up 
# with a nice config
# $CHANGE/handle-brightness-keys --create -t bool -s true
# SOURCE https://forum.xfce.org/viewtopic.php?id=12164
#
# Version : xfce power manger 1.6.6
# Date    : 30-APR-2021
#
# You can query the xfce4 settings of your choice with :
# xfconf-query -c xfce4-power-manager -lv
# xfconf-query -c thunar -lv
# xfconf-query -c thunar-volman -lv
# etc ...
# 
# The easiest way to get your settings is to go through all your settings in :
# thunar thunar-settings thunar-volman thunar-volman thunar-volman-settings
# xfce4-power-manager-settings etc ....
#
# Then run the settings above to get your settings and make your script that way
#
# SET VARIABLES
CHANGE="xfconf-query -c xfce4-power-manager -p /xfce4-power-manager"
CREATE="--create -t bool -s"
CREATE0="--create -s"
CREATE1="--create -t string -s"
#
# START THE POWER MANAGER
xfce4-power-manager &
#
# SET THE POWERMANAGER SETTINGS
$CHANGE/blank-on-ac $CREATE0 10
$CHANGE/blank-on-battery $CREATE0 5
$CHANGE/brightness-level-on-battery $CREATE0 10
$CHANGE/brightness-switch $CREATE0 0
$CHANGE/brightness-switch-restore-on-exit $CREATE0 0
$CHANGE/critical-power-action $CREATE0 3
$CHANGE/dpms-enabled $CREATE true
$CHANGE/dpms-on-ac-off $CREATE0 0
$CHANGE/dpms-on-ac-sleep $CREATE0 0
$CHANGE/dpms-on-battery-off $CREATE0 0
$CHANGE/dpms-on-battery-sleep $CREATE0 0
$CHANGE/general-notification $CREATE false
$CHANGE/handle-brightness-keys $CREATE true
$CHANGE/hibernate-button-action $CREATE0 3
$CHANGE/inactivity-on-battery $CREATE0 14
$CHANGE/lid-action-on-ac $CREATE0 0
$CHANGE/lid-action-on-battery $CREATE0 0
$CHANGE/lock-screen-suspend-hibernate $CREATE false
$CHANGE/logind-handle-lid-switch $CREATE false
$CHANGE/power-button-action $CREATE false
$CHANGE/presentation-mode $CREATE false
$CHANGE/show-panel-label $CREATE0 3
$CHANGE/show-tray-icon $CREATE false
