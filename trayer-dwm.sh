#!/bin/sh
# Trayer setup that I use with DWM
# This puts the trayer to the left of my dwm status. The position can be
# Changed with the "--distance" flag. 
# Version : 1.1.8
#
trayer --edge top --align right --widthtype request --height 20 --SetDockType true --SetPartialStrut true --transparent true --alpha 255 --tint 0x282c34 --distancefrom right --distance 418 --expand true --padding 1 --monitor 0 
