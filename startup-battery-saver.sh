#!/usr/bin/bash

if [ /usr/bin/xfce4-power-manager ] ;
then
     bash xfce4-power-manager-script.sh &
fi

if [ /usr/bin/thunar ] ;
then
	bash ~/bin/thunar.sh
fi

if [ /usr/bin/bash ] ;
then
     bash ~/bin/bar &
fi

if [ /usr/bin/bluetoothctl ] ;
then
     bluetoothctl power off &
fi

if [ /usr/bin/nmcli ] ;
then
	nmcli radio wifi off &
fi

